<?php

use Faker\Generator as Faker;

$factory->define(App\KategoriAkun::class, function (Faker $faker) {
    return [
        'nama_kategori' => $faker->unique()->sentence(2),
        'detail_kategori' => $faker->paragraphs(3, true)
    ];
});
