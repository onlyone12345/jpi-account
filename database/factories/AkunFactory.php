<?php

use Faker\Generator as Faker;

$factory->define(App\Akun::class, function (Faker $faker) {
    return [
        'nama_akun' => $faker->unique()->sentence(2),
        'kode_akun' => $faker->sentence(2),
        'detail_akun' => $faker->paragraphs(3, true),
        
    ];
});
