<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_perusahaan');
            $table->string('nama_pemilik');
            $table->string('nama_panggilan');
            $table->text('alamat_penagihan');
            $table->string('email');
            $table->string('telpon');
            $table->string('npwp');
            $table->integer('akun_hutang_id');
            $table->integer('akun_piutang_id');
            $table->text('informasi_lain');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
