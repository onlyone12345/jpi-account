<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_transaksi');
            $table->string('nama_transaksi');
            $table->integer('dari_akun_id');
            $table->integer('ke_akun_id');
            $table->integer('nominal');
            $table->text('memo');
            $table->date('tanggal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer_banks');
    }
}
