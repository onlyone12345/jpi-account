<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\KategoriAkun::class, 5)->create()->each(function($k){
            $k->akuns()
              ->saveMany(
                  factory(App\Akun::class, rand(1,5))->make()
              );
        });
    }
}
