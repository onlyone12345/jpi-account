@extends('layouts.theme')

@section('content')
@include('layouts._messages')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data Akun Kas & Bank</h3>
        <div class="pull-right">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                <span class="fa  fa-plus-square"></span>
                Buat Transaksi
            </button>
            <ul class="dropdown-menu">
                <li><a href="{{ route('transfer.create') }}">+ Tranasfer Uang</a></li>
                <li><a href="#">+ Terima Uang</a></li>
                <li><a href="#">+ Kirim Uang</a></li>
            </ul>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Kode Akun</th>
                    <th>Nama Akun</th>
                    <th>Kategori Akun</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                {{-- @foreach ($akuns as $akun)
                <tr>
                    <td>{{ $akun->kode_akun }}</td>
                    <td>{{ $akun->nama_akun }}</td>
                    <td>{{ $akun->kategoriakun->nama_kategori }}</td>
                    <td>0.00</td>
                </tr>
                @endforeach --}}
            </tbody>

        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
