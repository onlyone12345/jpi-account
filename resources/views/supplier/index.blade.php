@extends('layouts.theme')

@section('content')
@include('layouts._messages')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data Supplier</h3>
        <a href="{{ route('supplier.create') }}" class="btn btn-primary pull-right">
        <span class="fa  fa-plus-square"></span>        
        Tambah Supplier</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nama Panggilan</th>
                    <th>Alamat</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($suppliers as $supplier)
                <tr>
                    <td>{{ $supplier->nama_panggilan}}</td>
                    <td>{{ $supplier->alamat_penagihan }}</td>
                    <td>{{ $supplier->email }}</td>
                    <td>{{ $supplier->telpon }}</td>
                    <td>0.00</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
