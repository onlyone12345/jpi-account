@extends('layouts.theme')

@section('content')
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Tambah Produk') }}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="POST" class="form-horizontal" action="{{ route('produk.store') }}">
            @csrf
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="kode" class="col-sm-4 col-form-label text-md-right">Kode Produk</label>

                        <div class="col-md-6">
                            <input id="kode" type="text" class="form-control{{ $errors->has('kode') ? ' is-invalid' : '' }}"
                                name="kode" value="{{ old('kode') }}" autofocus>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="nama" class="col-sm-4 col-form-label text-md-right">Nama Produk</label>

                        <div class="col-md-6">
                            <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}"
                                name="nama" value="{{ old('nama') }}" autofocus>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="satuan" class="col-md-4 col-form-label text-md-right">Satuan</label>

                        <div class="col-md-6">
                            <input id="satuan" type="text" class="form-control{{ $errors->has('satuan') ? ' is-invalid' : '' }}"
                                name="satuan" value="{{ old('satuan') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="kategori" class="col-md-4 col-form-label text-md-right">Kategori</label>

                        <div class="col-md-6">
                            <input id="kategori" type="text" class="form-control{{ $errors->has('kategori') ? ' is-invalid' : '' }}"
                                name="kategori" value="{{ old('kategori') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="detail" class="col-md-4 col-form-label text-md-right">Detail</label>

                        <div class="col-md-6">
                            <textarea id="detail" class="form-control{{ $errors->has('detail') ? ' is-invalid' : '' }}"
                                rows="4" name="detail"> {{ old('detail') }} </textarea>
                        </div>
                    </div>

                    <!-- coment here -->
                </div>
            </div>
            <hr>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="harga_beli" class="col-sm-4 col-form-label text-md-right">Harga Beli</label>

                        <div class="col-md-6">
                            <input id="harga_beli" type="text" class="form-control{{ $errors->has('harga_beli') ? ' is-invalid' : '' }}"
                                name="harga_beli" value="{{ old('harga_beli') }}" placeholder="0.00">
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="akun_beli_id" class="col-md-4 col-form-label text-md-right">Akun Pembelian</label>

                        <div class="col-md-6">
                            <select id="akun_beli_id" name="akun_beli_id" class="form-control select2" style="width: 100%;">
                                @foreach ($akuns as $akun)
                                <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>

                                @endforeach

                            </select>

                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="harga_jual" class="col-sm-4 col-form-label text-md-right">Harga Jual</label>

                        <div class="col-md-6">
                            <input id="harga_jual" type="text" class="form-control{{ $errors->has('harga_juall') ? ' is-invalid' : '' }}"
                                name="harga_jual" value="{{ old('harga_jual') }}" placeholder="0.00">
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="akun_jual_id" class="col-md-4 col-form-label text-md-right">Akun Penjualan</label>

                        <div class="col-md-6">
                            <select id="akun_jual_id" name="akun_jual_id" class="form-control select2" style="width: 100%;">
                                @foreach ($akuns as $akun)
                                <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>

                                @endforeach

                            </select>

                        </div>
                    </div>
                </div>
            </div>

    <div class="box-footer">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                <span class="fa fa-plus"></span>
                {{ __('Tambah') }}

            </button>

        </div>
    </div>
    <!-- /.box-footer -->
    </form>
</div>
<!-- /.box -->
</div>

@endsection
