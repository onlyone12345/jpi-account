@extends('layouts.theme')

@section('content')
@include('layouts._messages')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data Aset</h3>
        <a href="{{ route('aset.create') }}" class="btn btn-primary pull-right">
        <span class="fa fa-plus-square"></span>        
        Tambah Aset</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Tanggal Akuisisi</th>
                    <th>Detail Aset</th>
                    <th>Akun Aset</th>
                    <th>Biaya Akuisisi</th>
                    <th>Nilai Baku</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($asets as $aset)
                <tr>
                    <td>{{ $aset->tanggal_akuisisi }}</td>
                    <td>{{ $aset->nama}}</td>
                    <td>{{ $aset->akun_aset->nama_akun}}</td>
                    <td>{{ $aset->biaya_akuisisi}}</td>
                    <td>{{ $aset->biaya_akuisisi}}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
