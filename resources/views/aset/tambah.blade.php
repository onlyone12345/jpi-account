@extends('layouts.theme')

@section('content')
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Tambah Aset') }}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="POST" class="form-horizontal" action="{{ route('aset.store') }}">
            @csrf
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="kode" class="col-sm-4 col-form-label text-md-right">Kode Aset</label>

                        <div class="col-md-6">
                            <input id="kode" type="text" class="form-control{{ $errors->has('kode') ? ' is-invalid' : '' }}"
                                name="kode" value="{{ old('kode') }}" autofocus>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="nama" class="col-sm-4 col-form-label text-md-right">Nama Aset</label>

                        <div class="col-md-6">
                            <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}"
                                name="nama" value="{{ old('nama') }}" autofocus>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="akun_aset_id" class="col-md-4 col-form-label text-md-right">Akun Aset Tetap</label>

                        <div class="col-md-6">
                            <select id="akun_aset_id" name="akun_aset_id" class="form-control select2" style="width: 100%;">
                                @foreach ($akuns as $akun)
                                <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>

                                @endforeach

                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="deskripsi" class="col-md-4 col-form-label text-md-right">Deskripsi</label>

                        <div class="col-md-6">
                            <textarea id="deskripsi" class="form-control{{ $errors->has('deskripsi') ? ' is-invalid' : '' }}"
                                rows="4" name="deskripsi"> {{ old('deskripsi') }} </textarea>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="tanggal_akuisisi" class="col-md-4 col-form-label text-md-right">Tanggal Akuisisi</label>

                        <div class="col-md-6">
                            <input id="tanggal_akuisisi" type="date" class="form-control{{ $errors->has('tanggal_akuisisi') ? ' is-invalid' : '' }}"
                                name="tanggal_akuisisi" value="{{ old('tanggal_akuisisi') }}">
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <label for="biaya_akuisisi" class="col-md-4 col-form-label text-md-right">Biaya Akuisisi</label>

                        <div class="col-md-6">
                            <input id="biaya_akuisisi" type="text" class="form-control{{ $errors->has('biaya_akuisisi') ? ' is-invalid' : '' }}"
                                name="biaya_akuisisi" value="{{ old('biaya_akuisisi') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                            <label for="akun_kredit_id" class="col-md-4 col-form-label text-md-right">Akun Dikreditkan</label>
    
                            <div class="col-md-6">
                                <select id="akun_kredit_id" name="akun_kredit_id" class="form-control select2" style="width: 100%;">
                                    @foreach ($akuns as $akun)
                                    <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>
    
                                    @endforeach
    
                                </select>
    
                            </div>
                        </div>

                   
                    <!-- coment here -->
                </div>
            </div>
            

            <div class="box-footer">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-plus"></span>
                        {{ __('Tambah') }}

                    </button>

                </div>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box -->
</div>

@endsection
