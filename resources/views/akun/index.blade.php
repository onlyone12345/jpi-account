@extends('layouts.theme')

@section('content')
@include('layouts._messages')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data Akun</h3>
        <a href="{{ route('akun.create') }}" class="btn btn-primary pull-right">
        <span class="fa  fa-plus-square"></span>        
        Buat Akun Baru</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Kode Akun</th>
                    <th>Nama Akun</th>
                    <th>Kategori Akun</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($akuns as $akun)
                <tr>
                    <td>{{ $akun->kode_akun }}</td>
                    <td>{{ $akun->nama_akun }}</td>
                    <td>{{ $akun->kategoriakun->nama_kategori }}</td>
                    <td>0.00</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
