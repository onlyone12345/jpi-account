@extends('layouts.theme')

@section('content')
<div class="col-md-6">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Tambah Akun') }}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="POST" class="form-horizontal" action="{{ route('akun.store') }}">
            @csrf
            <div class="box-body">
                <div class="form-group row">
                    <label for="kode_akun" class="col-sm-4 col-form-label text-md-right">Kode Akun</label>

                    <div class="col-md-6">
                        <input id="kode_akun" type="text" class="form-control{{ $errors->has('kode_akunl') ? ' is-invalid' : '' }}"
                            name="kode_akun" value="{{ old('kode_akun') }}" required autofocus>
                    </div>

                </div>
                <div class="form-group">
                    <label for="nama_akun" class="col-md-4 col-form-label text-md-right">Nama Akun</label>

                    <div class="col-md-6">
                        <input id="nama_akun" type="text" class="form-control{{ $errors->has('nama_akun') ? ' is-invalid' : '' }}"
                            name="nama_akun" value="{{ old('nama_akun') }}" required>

                    </div>
                </div>

                <div class="form-group">
                    <label for="detail_akun" class="col-md-4 col-form-label text-md-right">Detail Akun</label>

                    <div class="col-md-6">
                        <textarea id="detail_akun" class="form-control{{ $errors->has('detail_akun') ? ' is-invalid' : '' }}"
                            rows="4" name="detail_akun" required> {{ old('detail_akun') }} </textarea>

                    </div>
                </div>

                <div class="form-group">
                    <label for="kategori_akun_id" class="col-md-4 col-form-label text-md-right">Detail Akun</label>

                    <div class="col-md-6">
                        <select id="kategori_akun_id" name="kategori_akun_id" class="form-control select2" style="width: 100%;">
                            @foreach ($kategoriakuns as $kategoriakun)
                            <option value="{{ $kategoriakun->id }}">{{ $kategoriakun->nama_kategori}}</option>
                           
                            @endforeach
                            
                        </select>

                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-plus"></span>
                        {{ __('Tambah') }}

                    </button>

                </div>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box -->
</div>

@endsection
