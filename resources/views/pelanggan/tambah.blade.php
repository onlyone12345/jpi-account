@extends('layouts.theme')

@section('content')
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Tambah Pelanggan') }}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="POST" class="form-horizontal" action="{{ route('pelanggan.store') }}">
            @csrf
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="nama_perusahaan" class="col-sm-4 col-form-label text-md-right">Nama Perusahaan</label>

                        <div class="col-md-6">
                            <input id="nama_perusahaan" type="text" class="form-control{{ $errors->has('nama_perusahaan') ? ' is-invalid' : '' }}"
                                name="nama_perusahaan" value="{{ old('nama_perusahaan') }}" autofocus>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="nama_pemilik" class="col-md-4 col-form-label text-md-right">Nama pemilik</label>

                        <div class="col-md-6">
                            <input id="nama_pemilik" type="text" class="form-control{{ $errors->has('nama_pemilik') ? ' is-invalid' : '' }}"
                                name="nama_pemilik" value="{{ old('nama_pemilik') }}" >

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nama_panggilan" class="col-md-4 col-form-label text-md-right">Nama Panggilan *</label>

                        <div class="col-md-6">
                            <input id="nama_panggilan" type="text" class="form-control{{ $errors->has('nama_panggilan') ? ' is-invalid' : '' }}"
                                name="nama_panggilan" value="{{ old('nama_panggilan') }}" required>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="alamat_penagihan" class="col-md-4 col-form-label text-md-right">Alamat Penagihan</label>

                        <div class="col-md-6">
                            <textarea id="alamat_penagihan" class="form-control{{ $errors->has('alamat_penagihan') ? ' is-invalid' : '' }}"
                                rows="4" name="alamat_penagihan" > {{ old('alamat_penagihan') }} </textarea>

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="alamat_pengiriman" class="col-md-4 col-form-label text-md-right">Alamat Pengiriman</label>

                        <div class="col-md-6">
                            <textarea id="alamat_pengiriman" class="form-control{{ $errors->has('alamat_pengiriman') ? ' is-invalid' : '' }}"
                                rows="4" name="alamat_pengiriman" > {{ old('alamat_pengiriman') }} </textarea>

                        </div>
                    </div>


                    

                    <!-- coment here -->
                </div>

                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="telpon" class="col-sm-4 col-form-label text-md-right">Telpon</label>

                        <div class="col-md-6">
                            <input id="telpon" type="text" class="form-control{{ $errors->has('telponl') ? ' is-invalid' : '' }}"
                                name="telpon" value="{{ old('telpon') }}" >
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-sm-4 col-form-label text-md-right">Email</label>

                        <div class="col-md-6">
                            <input id="email" type="text" class="form-control{{ $errors->has('emaill') ? ' is-invalid' : '' }}"
                                name="email" value="{{ old('email') }}" s>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="npwp" class="col-sm-4 col-form-label text-md-right">NPWP</label>

                        <div class="col-md-6">
                            <input id="npwp" type="text" class="form-control{{ $errors->has('npwpl') ? ' is-invalid' : '' }}"
                                name="npwp" value="{{ old('npwp') }}" >
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="informasi_lain" class="col-md-4 col-form-label text-md-right">Informasi Lainnya</label>

                        <div class="col-md-6">
                            <textarea id="informasi_lain" class="form-control{{ $errors->has('informasi_lain') ? ' is-invalid' : '' }}"
                                rows="4" name="informasi_lain"> {{ old('informasi_lain') }} </textarea>

                        </div>

                    </div>

                    <div class="form-group">
                            <label for="akun_piutang_id" class="col-md-4 col-form-label text-md-right">Akun Piutang</label>
    
                            <div class="col-md-6">
                                <select id="akun_piutang_id" name="akun_piutang_id" class="form-control select2" style="width: 100%;">
                                    @foreach ($akuns as $akun)
                                    <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>
    
                                    @endforeach
    
                                </select>
    
                            </div>
                        </div>

                        <div class="form-group">
                                <label for="akun_hutang_id" class="col-md-4 col-form-label text-md-right">Akun Hutang</label>
        
                                <div class="col-md-6">
                                    <select id="akun_hutang_id" name="akun_hutang_id" class="form-control select2" style="width: 100%;">
                                        @foreach ($akuns as $akun)
                                        <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>
        
                                        @endforeach
        
                                    </select>
        
                                </div>
                            </div>




                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        <span class="fa fa-plus"></span>
                        {{ __('Tambah') }}

                    </button>

                </div>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box -->
</div>

@endsection
