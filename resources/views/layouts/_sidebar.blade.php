<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span><span class="pull-right-container"></a></li>
            <li><a href="#"><i class="fa fa-print"></i> <span>Laporan</span><span class="pull-right-container"></a></li>
            <hr>
            <li><a href="{{ route('kasbank') }}"><i class="fa fa-bank"></i> <span>Kas & Bank</span></a></li>
            <hr>
            <li><a href="{{ route('pelanggan.index') }}"><i class="fa fa-users"></i> <span>Pelanggan</span></a></li>
            <li><a href="{{ route('supplier.index') }}"><i class="fa fa-truck"></i> <span>Supplier</span></a></li>
            <li><a href="{{ route('produk.index') }}"><i class="fa fa-dropbox"></i> <span>Produk</span></a></li>
            <li><a href="{{ route('akun.index') }}"><i class="fa fa-book"></i> <span>Daftar Akun</span></a></li>
            <li><a href="{{ route('aset.index') }}"><i class="fa  fa-automobile"></i> <span>Daftar Aset</span></a></li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
