@extends('layouts.theme')

@section('content')
@include('layouts._messages')
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Data Produk</h3>
        <a href="{{ route('produk.create') }}" class="btn btn-primary pull-right">
        <span class="fa fa-plus-square"></span>        
        Tambah Produk</a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Kode Barang</th>
                    <th>Nama</th>
                    <th>Qty</th>
                    <th>Satuan</th>
                    <th>Harga Beli</th>
                    <th>Harga Jual</th>
                    <th>Kategori Produk</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($produks as $produk)
                <tr>
                    <td>{{ $produk->kode}}</td>
                    <td>{{ $produk->nama}}</td>
                    <td>0.00</td>
                    <td>{{ $produk->satuan}}</td>
                    <td>{{ $produk->harga_beli}}</td>
                    <td>{{ $produk->harga_jual}}</td>
                    <td>{{ $produk->kategori}}</td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
