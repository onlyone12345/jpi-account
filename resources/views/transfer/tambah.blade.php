@extends('layouts.theme')

@section('content')
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('Transfer Kas & Bank') }}</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form method="POST" class="form-horizontal" action="{{ route('transfer.store') }}">
            @csrf


            <div class="box-body">
                <div class="col-md-4" style="margin : 10px">
                    <div class="form-group">
                        <label for="dari_akun_id" class="col-form-label text-md-right">Transfer Dari</label>
                        <select id="dari_akun_id" name="dari_akun_id" class="form-control select2" style="width: 100%;">

                            @foreach ($akuns as $akun)
                            <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="col-md-4" style="margin : 10px">
                    <div class="form-group">
                        <label for="ke_akun_id" class="col-form-label text-md-right">Setor Ke</label>
                        <select id="ke_akun_id" name="ke_akun_id" class="form-control select2" style="width: 100%;">

                            @foreach ($akuns as $akun)
                            <option value="{{ $akun->id }}">{{ $akun->nama_akun}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>
                
                <div class="col-md-3" style="margin : 10px">
                    <div class="form-group row">
                        <label for="nominal" class="col-form-label text-md-right">Nominal</label>

                        <input id="nominal" type="text" class="form-control{{ $errors->has('nominal') ? ' is-invalid' : '' }}"
                            name="nominal" value="{{ old('nominal') }}" placeholder="0.00">

                    </div>
                </div>
            </div>
            <hr>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="kode_transaksi" class="col-sm-4 col-form-label text-md-right">Kode Transaksi</label>

                        <div class="col-md-6">
                            <input id="kode_transaksi" type="text" class="form-control{{ $errors->has('kode_transaksi') ? ' is-invalid' : '' }}"
                                name="kode_transaksi" value="{{ old('kode') }}" autofocus>
                        </div>

                    </div>

                    <div class="form-group row" hidden>
                            <label for="nama_transaksi" class="col-sm-4 col-form-label text-md-right">Nama Transaksi</label>
    
                            <div class="col-md-6">
                                <input id="nama_transaksi" type="text" class="form-control{{ $errors->has('nama_transaksi') ? ' is-invalid' : '' }}"
                                    name="nama_transaksi" value="Transfer Bank" autofocus>
                            </div>
    
                        </div>

                    <div class="form-group row">
                        <label for="tanggal" class="col-sm-4 col-form-label text-md-right">Tanggal Transaksi</label>

                        <div class="col-md-6">
                            <input id="tanggal" type="date" class="form-control{{ $errors->has('tanggal') ? ' is-invalid' : '' }}"
                                name="tanggal" value="{{ old('tanggal') }}" autofocus>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                   
                    <div class="form-group">
                        <label for="memo" class="col-md-4 col-form-label text-md-right">Memo / Keterangan</label>

                        <div class="col-md-6">
                            <textarea id="memo" class="form-control{{ $errors->has('memo') ? ' is-invalid' : '' }}"
                                rows="4" name="memo"> {{ old('memo') }} </textarea>
                        </div>
                    </div>

                    <!-- coment here -->
                </div>
            </div>
                    
            <div class="box-footer">
                <div class="col-md-12 offset-md-4">
                    <button type="submit" class="btn btn-primary pull-right">
                        <span class="fa fa-plus"></span>
                        {{ __('Simpan') }}

                    </button>

                </div>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box -->
</div>

@endsection
