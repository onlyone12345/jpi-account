<?php

namespace App\Http\Controllers;

use App\TransferBank;
use App\Akun;
use Illuminate\Http\Request;

class TransferBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akuns = Akun::all();
        return view('transfer.tambah', compact('akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transfer = TransferBank::create([
            'kode_transaksi' => request('kode_transaksi'),
            'nama_transaksi' => request('nama_transaksi'),
            'dari_akun_id' => request('dari_akun_id'),
            'ke_akun_id' => request('ke_akun_id'),
            'nominal' => request('nominal'),
            'memo' => request('memo'),
            'tanggal' => request('tanggal'),

       ]);
        // $request->akun()->create($request->only('kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'));
        
        //return redirect()->route('produk.index')->with('success', 'Data transaksi telah tersimpan');
        echo 'Berhasil Simpan';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TransferBank  $transferBank
     * @return \Illuminate\Http\Response
     */
    public function show(TransferBank $transferBank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TransferBank  $transferBank
     * @return \Illuminate\Http\Response
     */
    public function edit(TransferBank $transferBank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TransferBank  $transferBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransferBank $transferBank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TransferBank  $transferBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransferBank $transferBank)
    {
        //
    }
}
