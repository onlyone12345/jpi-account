<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Akun;
use App\KategoriAkun;

class AkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $akuns = Akun::all();

      // print_r($akuns);
       return view('akun.index', compact('akuns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akun = new Akun();
        $kategoriakuns = KategoriAkun::all();
        return view('akun.tambah', compact('kategoriakuns', 'akun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode_akun' => 'required',
            'nama_akun' => 'required',
            'detail_akun' => 'required',
            'kategori_akun_id' => 'required',
        ]);

        $akun = Akun::create([
            'kode_akun' => request('kode_akun'),
            'nama_akun' => request('nama_akun'),
            'detail_akun' => request('detail_akun'),
            'kategori_akun_id' => request('kategori_akun_id'),
       ]);
        // $request->akun()->create($request->only('kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'));
        
        return redirect()->route('akun.index')->with('success', 'Data aku telah tersimpan');

    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
