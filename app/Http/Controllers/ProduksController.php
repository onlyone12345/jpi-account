<?php

namespace App\Http\Controllers;

use App\Produk;
use App\Akun;
use Illuminate\Http\Request;

class ProduksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produks = Produk::all();
        return view('produk.index', compact('produks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akuns = Akun::all();
        return view('produk.tambah', compact('akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $produk = Produk::create([
            'kode' => request('kode'),
            'nama' => request('nama'),
            'satuan' => request('satuan'),
            'kategori' => request('kategori'),
            'detail' => request('detail'),
            'akun_beli_id' => request('akun_beli_id'),
            'akun_jual_id' => request('akun_jual_id'),
            'harga_beli' => request('harga_beli'),
            'harga_jual' => request('harga_jual'),
       ]);
        // $request->akun()->create($request->only('kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'));
        
        return redirect()->route('produk.index')->with('success', 'Data produk telah tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show(Produk $produk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit(Produk $produk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produk $produk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produk $produk)
    {
        //
    }
}
