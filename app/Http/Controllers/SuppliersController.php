<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Akun;
use Illuminate\Http\Request;

class SuppliersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::all();
        return view('supplier.index', compact('suppliers'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akuns = Akun::all();
        return view('supplier.tambah', compact('akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_panggilan' => 'required',
        ]);

        $akun = Supplier::create([
            'nama_perusahaan' => request('nama_perusahaan'),
            'nama_pemilik' => request('nama_pemilik'),
            'nama_panggilan' => request('nama_panggilan'),
            'alamat_penagihan' => request('alamat_penagihan'),
            'telpon' => request('telpon'),
            'email' => request('email'),
            'npwp' => request('npwp'),
            'informasi_lain' => request('informasi_lain'),
            'akun_hutang_id' => request('akun_hutang_id'),
            'akun_piutang_id' => request('akun_piutang_id'),
       ]);
        // $request->akun()->create($request->only('kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'));
        
        return redirect()->route('supplier.index')->with('success', 'Data supplier telah tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }
}
