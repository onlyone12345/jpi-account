<?php

namespace App\Http\Controllers;

use App\KategoriAkun;
use Illuminate\Http\Request;

class KategoriAkunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KategoriAkun  $kategoriAkun
     * @return \Illuminate\Http\Response
     */
    public function show(KategoriAkun $kategoriAkun)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KategoriAkun  $kategoriAkun
     * @return \Illuminate\Http\Response
     */
    public function edit(KategoriAkun $kategoriAkun)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KategoriAkun  $kategoriAkun
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KategoriAkun $kategoriAkun)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KategoriAkun  $kategoriAkun
     * @return \Illuminate\Http\Response
     */
    public function destroy(KategoriAkun $kategoriAkun)
    {
        //
    }
}
