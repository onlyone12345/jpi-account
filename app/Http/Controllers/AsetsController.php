<?php

namespace App\Http\Controllers;

use App\Aset;
use App\Akun;
use Illuminate\Http\Request;

class AsetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asets = Aset::all();

      // print_r($akuns);
       return view('aset.index', compact('asets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akuns = Akun::all();
        return view('aset.tambah', compact('akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aset = Aset::create([
            'kode' => request('kode'),
            'nama' => request('nama'),
            'akun_aset_id' => request('akun_aset_id'),
            'deskripsi' => request('deskripsi'),
            'tanggal_akuisisi' => request('tanggal_akuisisi'),
            'biaya_akuisisi' => request('biaya_akuisisi'),
            'akun_kredit_id' => request('akun_kredit_id'),

       ]);
        // $request->akun()->create($request->only('kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'));
        
        return redirect()->route('aset.index')->with('success', 'Data aset telah tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function show(Aset $aset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function edit(Aset $aset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Aset $aset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Aset $aset)
    {
        //
    }
}
