<?php

namespace App\Http\Controllers;

use App\Pelanggan;
use App\Akun;
use Illuminate\Http\Request;

class PelanggansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelanggans = Pelanggan::all();
        return view('pelanggan.index', compact('pelanggans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akuns = Akun::all();
        return view('pelanggan.tambah', compact('akuns'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_panggilan' => 'required',
        ]);

        $pelanggan = Pelanggan::create([
            'nama_perusahaan' => request('nama_perusahaan'),
            'nama_pemilik' => request('nama_pemilik'),
            'nama_panggilan' => request('nama_panggilan'),
            'alamat_pengiriman' => request('alamat_pengiriman'),
            'alamat_penagihan' => request('alamat_penagihan'),
            'telpon' => request('telpon'),
            'email' => request('email'),
            'npwp' => request('npwp'),
            'informasi_lain' => request('informasi_lain'),
            'akun_hutang_id' => request('akun_hutang_id'),
            'akun_piutang_id' => request('akun_piutang_id'),
       ]);
        // $request->akun()->create($request->only('kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'));
        
        return redirect()->route('pelanggan.index')->with('success', 'Data pelanggan telah tersimpan');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function show(Pelanggan $pelanggan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pelanggan $pelanggan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pelanggan $pelanggan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pelanggan  $pelanggan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pelanggan $pelanggan)
    {
        //
    }
}
