<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransferBank extends Model
{
    protected $fillable = ['kode_transaksi', 'nama_transaksi', 'dari_akun_id', 'ke_akun_id', 'nominal', 'memo', 'tanggal'];
}
