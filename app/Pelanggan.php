<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
   protected $fillable = ['nama_perusahaan', 'nama_pemilik', 'nama_panggilan', 'alamat_pengiriman', 'alamat_penagihan', 'telpon', 'email', 'npwp', 'informasi_lain', 'akun_piutang_id', 'akun_hutang_id'];
}
