<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriAkun extends Model
{
    protected $fillable = ['nama_kategori', 'detail_kategori'];

    public function akun()
    {
        return $this->hasMany('Akun');
    }
}
