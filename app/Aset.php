<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aset extends Model
{
    protected $fillable = ['kode', 'nama', 'akun_aset_id', 'deskripsi', 'tanggal_akuisisi', 'biaya_akuisisi', 'akun_kredit_id'];

    public function akun_aset()
    {
        return $this->belongsTo(Akun::class, 'akun_aset_id');
    }
}
