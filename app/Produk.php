<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $fillable = ['kode', 'nama', 'satuan', 'kategori', 'detail', 'akun_beli_id', 'akun_jual_id', 'harga_jual', 'harga_beli'];
    
}
