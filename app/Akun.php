<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Akun extends Model
{
    protected $fillable = ['kode_akun', 'nama_akun', 'detail_akun', 'kategori_akun_id'];

    public function kategoriakun()
    {
        return $this->belongsTo(KategoriAkun::class, 'kategori_akun_id');
    }
   
}
