<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cek', function () {
    return view('layouts.theme');
});


Route::resource('akun', 'AkunController');

Route::resource('pelanggan', 'PelanggansController');

Route::resource('supplier', 'SuppliersController');

Route::resource('produk', 'ProduksController');

Route::resource('aset', 'AsetsController');

Route::get('/kasbank', 'KasBankController@index')->name('kasbank');

Route::resource('transfer', 'TransferBankController');